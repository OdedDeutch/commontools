/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.commonTools;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class JSONLogger implements ILogger {  
    private final JSONWriter writer;
    
    public JSONLogger(String path, String fileName) throws IOException {
        File f = new File(path);
            if(!f.exists()) {
                f.mkdirs();
            }
        this.writer = new JSONWriter(path, fileName);
    }
    
    @Override
    public void WriteLn(String line) {
        try {
            writer.writeLine(line);
        } catch (IOException ex) {
            Logger.getLogger(JSONLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void close() {
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(JSONLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private class JSONWriter {
    AsynchronousFileChannel fileChannel;
    CompletionHandler<Integer, Object> handler;
    
        public JSONWriter(String path, String fileName) throws IOException { 
            fileChannel = AsynchronousFileChannel.open(
                Paths.get(path + "/" + fileName), StandardOpenOption.READ,
                StandardOpenOption.WRITE, StandardOpenOption.CREATE);
            
            handler = new CompletionHandler<Integer, Object>() {

                @Override
                public void completed(Integer result, Object attachment) {}

                @Override
                public void failed(Throwable e, Object attachment) {}
              };
        }

        public void writeLine(String str) throws IOException {
            String line = str + "\n";
            fileChannel.write(ByteBuffer.wrap(line.getBytes()), fileChannel.size(), null, handler);
        }

        public void close() throws IOException {
            fileChannel.close();
        }
    }
}
