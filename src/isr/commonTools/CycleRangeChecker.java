/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.commonTools;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class CycleRangeChecker {
    private final int top;
    private final int bottom;
    private final int cycleRange;

    public CycleRangeChecker(int top, int bottom, int cycleRange) {
        if(cycleRange <= 0)
            throw new IllegalArgumentException("CycleRange cannot be equel or less then 0");
        
        if(top <= bottom)
            throw new IllegalArgumentException("Top must be bigger then Bottom or less then 0");
        
        this.top = top;
        this.bottom = bottom;
        this.cycleRange = cycleRange;
    }
    
    public int getDiff(int current, int check) {
        int ret = check - current;
        
        
        return ret;
    }

    public boolean isBetweanRange(int current, int check) {
        if(check(current, check))
            return true;

        if(current <= check)
            current += cycleRange;
        else
            check += cycleRange;

        return check(current, check);
    }

    private boolean check(int current, int check) {
        return (current + top >= check) && (current - bottom <= check);
    }
}
