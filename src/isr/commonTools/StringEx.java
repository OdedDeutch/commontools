/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.commonTools;
/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class StringEx {
    
    public static final String EMPTY = "";
    
    public static String encrypt(String value, String token) {
        StringBuilder strDataOut = new StringBuilder(); 
        
        for(int index = 0; index < value.length(); index++) {
            int intXOrValue1 = (int)value.charAt(index);   
            int intXOrValue2 = (int)token.charAt((index +1) % token.length());
            
            String tempString = Integer.toHexString(
                    intXOrValue1 ^ (intXOrValue2 & 0xf));

            if(tempString.length() == 1) 
                tempString = "0" + tempString;
                     
            strDataOut.append(tempString);
        }
        return strDataOut.toString();
    }
    
    public static String decrypt(String value, String token) {       
        StringBuilder strDataOut = new StringBuilder();
        
        for(int index = 0; index < (value.length() / 2); index++) {
            int intXOrValue1 = (int)Long.parseLong(
                    value.substring(index * 2, index * 2 + 2), 16);
            
            int intXOrValue2 = (int)token.charAt((index +1) % token.length());
            strDataOut.append((char)(intXOrValue1 ^ (intXOrValue2 & 0xf)));
        }
        return strDataOut.toString();
    }
    
    public static boolean isNullOrEmpty(String str) {
        return str == null || str.length() == 0;
    }
    
    public static boolean equals(String str1, String str2) {
        return str1.equals(str2);
    }
}
