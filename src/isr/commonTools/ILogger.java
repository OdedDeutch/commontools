/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.commonTools;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public interface ILogger {
    void WriteLn(String msg);
    
    void close();
}
