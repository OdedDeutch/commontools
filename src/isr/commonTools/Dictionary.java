/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.commonTools;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public class Dictionary {
        
    public static Map<String, String> getDictonary(String line, String outerDelimiter, String innerDelimiter) {
        Map<String, String> map = new HashMap<>();
        
        String[] strings = line.split(outerDelimiter);
        for(String str : strings) {
            String[] curr = str.split(innerDelimiter);
            switch(curr.length) {
                case(1) :
                    map.put(curr[0], StringEx.EMPTY);
                    break;
                case(2):
                    map.put(curr[0], curr[1]);
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Key %s contains more the 1 values", curr[0]));     
            }
        }
        return map;
    }
}
