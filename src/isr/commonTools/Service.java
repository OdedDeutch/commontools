/*
 * Copyright 2015 ISR Corp - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential.
 * http://isrcorp.co.il/
 */
package isr.commonTools;

/**
 *
 * @author Oded Deutch <Odedd@isrcorp.co.il>
 */
public enum Service {
    CreditService("PC"), 
    TokenService("TN"), 
    MonitoringService("MN");
    
    final private String name;
    
    private Service(String name) {
        this.name = name;
    }
    
    public static Service convert(String name) {
        switch(name) {
            case "PC":
                return CreditService;
            case "TN":
                return TokenService;
            default:
                return MonitoringService;
        }
    }
    
    public String getName() {
        return name;
    }
}
